# Welcome to YouPlay

Do you like music but not restrictions?

With Youplay you can search for whatever music song in your mind. It will list a number of matching songs. You can listen and download it as an mp3 file.

Youplay is a single Python file with little dependencies. It is also suitable for beginners, who want to learn Python.

## Installation

It is intended to provide YouPlay as a Flatpak for easy installation and updates. Until the Flatpak is packed, the code is available on: [Codeberg.org](https://codeberg.org/ralfhersel/youplay.git)

* youplay.py - the python script

* youplay.svg - the app icon

* youplay.desktop - the GNOME launcher

* README.md - this file

* youplay.png - the logo

Just download the files to a dedicated folder, eg. /home/username/youplay/. Make youplay.py executable (chmod +x youplay.py). Change the pathes in youplay.desktop to your pathes. There are lines for Exec, Icon and Path, that must be adapted to your local directories. Copy the file youplay.desktop to /home/user/.local/share/applications/ to make it visible to the launcher of your desktop environment. Check if the launcher icon is able to start the program.

## Dependencies

YouPlay has some dependencies, which are:

* python3 (required to run a python script)
  should already be part of your distribution

* mpv (required for the CLI player)
  install with eg.: sudo apt install mpv

* python-mpv (required for the GUI player)
  install with: pip3 install python-mpv

* youtube-dl (required to search and download music from Youtube)
  install with: pip3 install --upgrade youtube-dl

Please install these dependencies before you start YouPlay.

## Operation Instructions

YouPlay supports two modes: CLI and GUI

You can start YouPlay in CLI mode like this:

* ./youplay.py

* ./youplay.py songtitle

All other options will be shown interactively. In this mode, the audio player MPV will be shown in CLI-mode as well.

You can also start YouPlay in GUI mode:

* ./youplay.py gui
* youplay.desktop (with adapted pathes and copied to /home/user/.local/share/applications/)

When the GUI shows up, you can enter a song title that you want to listen to. The App will present a list of 10 matching songs from Youtube. Select one or doubleclick to start downloading and playing it. In GUI mode, YouPlay will start MPV in internal mode (no GUI). All songs that you are listened to, are stored as mp3 files in the 'music' subfolder.

A song, that was already downloaded, will not be downloaded again, as long as the file exists in the 'music' subfolder.

## Release Notes

###### Version 0.22 - 2020.12.21

* GUI mode will utilize the internal python-mpv player instead of the external mpv player

* PLAY-button will be disabled if you doubleclick song title or click the PLAY-button

###### Version 0.21 - 2020.12.19 (my birthday version)

* ffmpeg dependency removed, because youtube-dl will download mp3 directly

* Downloaded songs will be stored in the 'music' subfolder

* Already downloaded songs will not be downloaded again, but played immediately

* Option to save a song removed from CLI and GUI, because it is obsolete

* PLAY button disabled when song is playing (only when started from PLAY button, not from doubleclick)

###### Version 0.20 - 2020.12.18

* Info about song duration added to CLI and GUI

* Endless streams are omitted (by duration = 0)

* Number of songs reduced to 10 (to avoid lenghty loading time)

###### Version 0.19 - 2020.12.16

* Progress bar while searching, downloading and saving songs
* Progress bar for CLI and GUI mode

###### Version 0.18 - 2020.12.15

* youplay.desktop-file doesn't require bash-script, but call youplay.py directly

* CLI-mode utilises MPV in CLI-mode

* GUI-mode starts MPV in GUI-mode

* Doubleclick in song list, starts download and playback of selected song

## Todo

* avoid youplay.desktop to launch two icons

* align columms in GUI-mode

* add tab for downloaded songs

## Licensing

Author: Ralf Hersel

License: GPL3

Repository: https://codeberg.org/ralfhersel/youplay.git

## Contact

https://matrix.to/#/@ralfhersel:feneas.org
