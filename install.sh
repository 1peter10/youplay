#!/bin/bash
# Getting your username and adjusting .desktop
username=$(whoami)
sed -i "s/username/${username}/g" youplay.desktop
mkdir -p ~/.local/opt/youplay
cp youplay.* ~/.local/opt/youplay/
cp youplay.desktop ~/.local/share/applications/
# install dependencies
pip3 install --user --upgrade youtube-dl
pip3 install --user --upgrade python-mpv
# adding a symlink in your Music folder
ln -s ~/.local/opt/youplay/music ~/Music/youplay
# adding a symlink to ~/.local/bin/ to be able to run YouPlay on cli with youplay.py
# (~./local/bin needs to be in your PATH for this to work)
ln -s ~/.local/opt/youplay/youplay.py ~/.local/bin/youplay.py
echo "If you did not see any errors, this should have worked. Make sure to have mpv installed." 
