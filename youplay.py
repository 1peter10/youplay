#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Name:           youplay.py
Description:    search, play and save audio files from Youtube
Usage:          start it with: './youplay.py' or './youplay.py song' or './youplay.py gui'
Author:         Ralf Hersel - ralf.hersel@gmx.net
Licence:        GPL3
More infos:     see constants and readme.md
'''

# === Todo =====================================================================

'''
- align column rendering
- disable Play when playing
- implement python-mpv
- avoid download of existing song

'''

# === Dependencies =============================================================

'''
python3    - default on most GNU/Linux-Distros                                  # because it is Python :)
youtube-dl - pip3 install --upgrade youtube-dl                                  # search, download from Youtube
mpv        - sudo apt install mpv                                               # stand-alone player
python-mpv - pip3 install python-mpv                                            # python bindings for the integrated MPV player
'''

# === Libraries ================================================================

import subprocess
import os
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GLib
import time
import sys
import mpv


# === Constants ================================================================

PROG_NAME        = 'YouPlay'
VERSION          = '0.22'
LAST_UPDATE      = '2020-12-21'
LICENCE          = 'GPL3'
AUTHOR           = 'Ralf Hersel'
WIN_WIDTH        = 750
WIN_HEIGHT       = 373
LINE             = '-' * 80
NUMBER_OF_SONGS  = 10
MAX_TITLE_LENGHT = 80

WELCOME = '''
Welcome to {} - Version {}
================================================================================
'''.format(PROG_NAME, VERSION)

CONTROLS = '''
Player controls:
    q       quit
    space   pause/continue
    right   fast forward
    left    fast backward
'''

THANKS = '''
================================================================================
Thank you for using {}!
'''.format(PROG_NAME)


# === Features =================================================================

def get_songlist(song):                                                         # get songlist from Youtube
    filename = 'audio.list'
    print('Retrieving song list from Youtube:', song, '\nPlease wait ', end='')
    command = 'youtube-dl --get-title --get-duration "ytsearch{}:{}" > {}'.format(str(NUMBER_OF_SONGS), song, filename)
    p = subprocess.Popen(command, shell=True)
    wait_until_process_finished(p)
    song_dict = {}
    f = open(filename)
    count = 1                                                                   # file line counter
    number = 1                                                                  # list line counter
    for line in f:
        striped_line = line.strip()
        if count % 2 == 0:                                                      # even = duration
            duration = striped_line
            if duration != '0':                                                 # no stream
                song_dict[str(number)] = [title, duration]
                number += 1
        else:                                                                   # not even = titel
            title = striped_line[:MAX_TITLE_LENGHT]                             # restrict to 100 chars
        count += 1
    f.close()
    return song_dict


def show_list(song_dict):                                                       # show the list of songs
    print(LINE)
    for key, item in song_dict.items():
        duration = item[1]
        print(str(key).ljust(3), item[0], '-', item[1])
    print(LINE)
    keep_asking = True
    while keep_asking:
        answer = input('\nSong number or RETURN for first song or (q)uit: ')
        if answer == 'q':                                                       # quit
            keep_asking = False
        elif len(answer) == 0:                                                  # return
            answer = '1'                                                        # play first song
            keep_asking = False
        elif not answer.isdigit():                                              # wrong letter
            print('WARNING: wrong entry, Ty again.')
        elif int(answer) in range(1, NUMBER_OF_SONGS + 1):                      # check number
            keep_asking = False
        else:
            print('WARNING: wrong number. Try again.')
    return answer        


def get_song(song):                                                             # Get song from Youtube and download it
    if not os.path.exists('music'):                                             # create sub-dir 'music' if not exists
        os.mkdir('music')
    song_list = song.split()
    audio_filename = '_'.join(song_list) + '.mp3'                               # construct filename
    audio_filename = sanitize_filename(audio_filename)
    audio_path = 'music/' + audio_filename
    if not os.path.exists(audio_path):
        print('\nDownloading song from Youtube:', song, '\nPlease wait until the song is downloaded ', end='')
        p = subprocess.Popen(['youtube-dl', '-q', '-f bestaudio', '--max-downloads',
         '1', '--yes-playlist', '--default-search', 'ytsearch', song,
         '--audio-format', 'mp3', '-o', audio_path])
        wait_until_process_finished(p)
    return audio_path
    

def play_song(audio_path):                                                      # play song from CLI
    print(CONTROLS)
    p = subprocess.Popen(['mpv', audio_path])                                   # play the song with MPV
    p.wait()
    p.terminate()


# === Misc =====================================================================

def sanitize_filename(filename):                                                # create proper filename
    sanitized_filename = ''.join( x for x in filename if (x.isalnum() or x in "._- "))
    return sanitized_filename


def wait_until_process_finished(p):                                             # do things until process finished
    while p.poll() is None:                                                     # subprosses still running?
        print('.', end='')                                                      # CLI progress bar
        sys.stdout.flush()                                                      # update display
        now = time.time()                                                       # next 3 lines replaces 'sleep(1)'
        while time.time() - now < 1:
            Gtk.main_iteration_do(False)
    print()                                                                     # add break to finish breakless print


#~ === GUI =====================================================================

class Gui:

    def __init__(self):
        # === Variables ========================================================
        
        self.player = mpv.MPV(ytdl=True)                                        # initialize mpv internal player
        
        # === Widgets ==========================================================

        # Window
            # Box (vertical)
                # Entry
                # ScrolledWindow
                    # TreeView
                        # TreeViewColumn
                # Box (horizontal)
                    # Button
                # StatusBar
                # ProgressBar
        
        self.window = Gtk.Window()                                              # Window
        self.window.set_title(PROG_NAME + ' it from Youtube')
        self.window.set_default_icon_from_file('youplay.svg')
        self.window.set_default_size(WIN_WIDTH, WIN_HEIGHT)                     # width, height
        self.window.connect('delete_event', self.on_window_delete)              # Window red-cross clicked
        
        self.main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)           # Vertical Box
        self.window.add(self.main_box)
        
        self.entry = Gtk.Entry()                                                # Entry
        self.entry.connect('activate', self.on_entry_return)                    # Return pressed
        self.entry.set_text('Enter song name')
        self.main_box.pack_start(self.entry, False, False, 0)
        
        self.liststore = Gtk.ListStore(str, str, str)                           # Treeview and Liststore
        self.treeview = Gtk.TreeView(model=self.liststore)
        self.treeview.connect('button-press-event', self.on_treeview_clicked)	# Treeview item clicked
        for i, column_title in enumerate(['Nr', 'Title', 'Duration']):
            renderer = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(column_title, renderer, text=i)
            self.treeview.append_column(column)
            
        self.scrollable_treelist = Gtk.ScrolledWindow()
        self.scrollable_treelist.add(self.treeview)
        self.main_box.pack_start(self.scrollable_treelist, True, True, 0)
        
        self.button_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)       # Horizontal Box
        for button_label in ['Play', 'Stop', 'About', 'Quit']:                  # button bar
            self.button = Gtk.Button(label=button_label)
            self.button.connect('clicked', self.on_button_clicked)
            self.button_box.pack_start(self.button, True, True, 0)
        self.main_box.add(self.button_box)
        
        self.status_bar = Gtk.Statusbar()
        self.main_box.add(self.status_bar)

        self.progressbar = Gtk.ProgressBar()
        self.progressbar.set_pulse_step(0.2)
        self.timeout_id = GLib.timeout_add(200, self.on_timeout, None)          # progress bar speed
        self.main_box.add(self.progressbar)
        
        self.window.show_all()                                                  # show GUI
        self.progressbar.hide()


# === Event Handler ============================================================

    def on_window_delete(self, widget, event, data=None):                       # close the window by red cross
        self.player.stop()
        Gtk.main_quit()


    def on_entry_return(self, widget):                                          # Return pressed in entry field
        self.progressbar.show()
        self.show_status_text('Retrieving song list from Youtube ... please wait')
        song = widget.get_text()
        song_dict = get_songlist(song)
        song_list = song_dict.items()                                           # convert dict to list
        if len(song_list) == 0:                                                 # nothing found
            self.show_status_text('Nothing found on Youtube. Try another search.')
        else:
            self.liststore.clear()
            for key, item in song_dict.items():
                number = key
                title = item[0]
                duration = item[1]
                self.liststore.append([number, title, duration])
            self.show_status_text('Select a song and click Play')
        self.progressbar.hide()


    def on_treeview_clicked(self, widget, event):                               # click on treeview item
        if event.type == Gdk.EventType._2BUTTON_PRESS:                          # but only doubleclick
            selected = self.get_tree_selection(self.treeview)
            if self.get_button_status('Play'):                                  # PLAY button enabled
                self.toggle_button_on_off('Play', False)                        # PLAY button disable
                self.do_play(selected, widget)


    def on_button_clicked(self, widget):                                        # do whatever the button says
        command = widget.get_label()
        selected = self.get_tree_selection(self.treeview)

        if command == 'Play':
            widget.set_sensitive(False)
            self.do_play(selected, widget)
                
        elif command == 'Stop':
            self.player.stop()
            self.toggle_button_on_off('Play', True)
            self.show_status_text('Stopped playing')
        
        elif command == 'About':
            text = '''
            Reclaim Freedom
            Suck it from Youtube
            Fight against digital handcuffs
            
            Version    : {}
            Update     : {}
            Licence    : {}
            Author     : {}
            Contact    : @ralfhersel:feneas.org

            Stay free, play music, donate artists
            '''.format(VERSION, LAST_UPDATE, LICENCE, AUTHOR)
            self.show_info_dialog(PROG_NAME, text)

        elif command == 'Quit':
            self.player.stop()
            Gtk.main_quit()


    def on_timeout(self, user_data):                                            # update value on progress bar
        self.progressbar.pulse()
        return True


# === Methods ==================================================================

    def do_play(self, selected, play_button):                                   # play song from GUI
        if selected == None:
            self.show_status_text('Nothing selected')
            play_button.set_sensitive(True)
        else:
            self.progressbar.show()
            self.show_status_text('Downloading song from Youtube ... please wait')
            song = selected[1]
            audio_path = get_song(song)
            self.progressbar.hide()
            self.show_status_text('Playing: ' + selected[1])
            self.player.play(audio_path)


    def get_tree_selection(self, tree):                                         # get selected tree item
        selection = tree.get_selection()
        model, treeiter = selection.get_selected()
        if treeiter != None:
            result = model[treeiter]
        else:
            result = None
        return result
        
    
    def show_status_text(self, text):                                           # show status text
        self.status_bar.push(0, text)
        while Gtk.events_pending():
            Gtk.main_iteration_do(True)                                         # wait until statusbar is refreshed


    def show_yesno_dialog(self, title, text):                                   # Dialog with Yes/No button
        dialog = Gtk.MessageDialog(message_type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.YES_NO, text=text)
        dialog.set_title(title)
        resp = dialog.run()
        dialog.destroy()
        if resp == Gtk.ResponseType.YES:
            return True
        else:
            return False


    def show_info_dialog(self, title, text):                                    # Info dialog
        dialog = Gtk.MessageDialog(message_type=Gtk.MessageType.INFO,
            buttons=Gtk.ButtonsType.OK, text=text)
        dialog.set_title(title)
        dialog.run()
        dialog.destroy()


    def toggle_button_on_off(self, button_label, true_false):                   # enable/disable button
        for button in self.button_box.get_children():
            if button.get_label() == button_label:
                button.set_sensitive(true_false)
                break
        return button.get_sensitive()


    def get_button_status(self, button_label):                                  # return, if button is enabled or disabled
        for button in self.button_box.get_children():
            if button.get_label() == button_label:
                return button.get_sensitive()


# === Main =====================================================================

def main(args):
    print(WELCOME)
    song = ' '.join(args[1:])                                                   # concat song title from args
    song_dict = {}
    answer = ''
    question = 'Enter (q)uit or song title: '
    finish = False
    while finish == False:
        if song == 'gui':                                                       # show GUI
            gui = Gui()
            Gtk.main()
            finish = True
        elif song == '':
            song = input(question)                                              # no song from command line
            if song == '':
                print('Error: wrong entry')
                finish = True
            elif song == 'q':
                finish = True
        elif song == 'q':                                                       # quit
            song = input(question)
            if song == 'q':
                finish = True
        else:    
            if song != 'l':                                                     # don't show list again
                song_dict = get_songlist(song)
                question = 'Enter (q)uit or (l)ist or song title: '
            if len(song_dict) > 0:                                              # list not empty
                answer = show_list(song_dict)
            if answer == 'q':
                song = 'q'
            else:
                if len(song_dict) == 0:                                         # nothing found
                    print('Warning: Youtube found nothing')
                    song = 'q'
                else:
                    song = song_dict[answer][0]                                 # get song name
                    audio_path = get_song(song)                                 # download song
                    play_song(audio_path)                                       # play song
                    song = ''
    print(THANKS)
    return 0
    

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
